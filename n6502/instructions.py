import numpy as np

def INS_JSR(cpu):
    """JSR - Jump to Subroutine - 0x20

    Args:
        cpu (n6502.CPU): The cpu object.
    """
    subAddress = cpu.fetchWord()
    cpu.writeWordToStack(cpu.PC-1)
    cpu.setRegister("PC", subAddress)

def INS_RTS(cpu):
    """RTS - Return from Subroutine - 0x60

    Args:
        cpu (n6502.CPU): The cpu object.
    """
    returnAddress = cpu.popWordFromStack()
    cpu.setRegister("PC", returnAddress)

def INS_LDA_IM(cpu):
    """LDA Imediate - 0x49

    Args:
        cpu (n6502.CPU): The cpu object.
    """
    byte = cpu.fetchByte()
    cpu.setRegister("A", byte)
    LDA_set_status(cpu)

def INS_LDA_ZP(cpu):
    """LDA Zero Page - 0xA5

    Args:
        cpu (n6502.CPU): The cpu object.
    """
    ZeroPageAddress = cpu.fetchByte()
    byte = cpu.readByte(ZeroPageAddress)
    cpu.setRegister("A", byte)
    LDA_set_status(cpu)

def INS_LDA_ZPX(cpu):
    """LDA Zero Page X - 0xB5

    Args:
        cpu (n6502.CPU): The cpu object.
    """
    ZeroPageAddress = cpu.fetchByte()
    ZeroPageAddress += cpu.getRegister("X")
    byte = cpu.readByte(ZeroPageAddress)
    cpu.setRegister("A", byte)
    LDA_set_status(cpu)

def LDA_set_status(cpu):
    """Sets all processor flags for LDA instructions.

    Args:
        cpu (n6502.CPU): The cpu object.
    """
    cpu.Z = cpu.A == 0
    cpu.N = bool(np.unpackbits(np.uint8(cpu.A))[7])

CPU_OPT_CODES = {
    0x20: INS_JSR,
    0x60: INS_RTS,
    0xA5: INS_LDA_ZP,
    0xA9: INS_LDA_IM,
    0xB5: INS_LDA_ZPX
}