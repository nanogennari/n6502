import numpy as np

class MEM:
    """Class to store the memory."""
    def __init__(self, size=1024*64):
        """Instances a memory object.

        Args:
            size (int, optional): Size of the memory. Defaults to 1024*64.
        """
        self.MAX_MEM = size
        self.data = np.zeros(self.MAX_MEM, dtype=np.uint8)

    # Erase all memory
    def initialize(self):
        """Erases all memory"""
        self.data[:] = 0

    # Read one byte
    def __getitem__(self, address):
        """Read one byte.

        Usage: memory[0xFF00]

        Args:
            address (np.uint16): Address to be accessed.

        Returns:
            np.uint8: Byte on address.
        """
        if address >= self.MAX_MEM:
            print("Tried to read out of memory address {}".format(hex(address)))
            return 0x00
        return self.data[address]

    # Write one byte
    def __setitem__(self, address, value):
        """Writes one byte to memory.

        Args:
            address (np.uint16): Address to be written.
            value (np.uint8): Byte to be written.
        """
        if address >= self.MAX_MEM:
            print("Tried to set out of memory address {}".format(hex(address)))
        self.data[address] = value