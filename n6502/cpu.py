import numpy as np
from .instructions import CPU_OPT_CODES

class CPU:
    def __init__(self, memory):
        # Pointers
        self.PC = np.uint16(0)
        self.SP = np.uint16(0)

        # Registers
        self.A = np.uint8(0)
        self.X = np.uint8(0)
        self.Y = np.uint8(0)

        # Status Flag
        self.C = np.bool(0)
        self.Z = np.bool(0)
        self.D = np.bool(0)
        self.B = np.bool(0)
        self.V = np.bool(0)
        self.N = np.bool(0)

        self.availableRegisters = ["PC", "SC", "A", "X", "Y"]
        self.cycles_count = 0
        self.LITTLE_ENDIAN = True

        self.memory = memory

    def reset(self):
        self.PC = 0xFFFC
        self.SP = 0x0100
        self.A = 0
        self.X = 0
        self.Y = 0
        self.C = False
        self.Z = False
        self.D = False
        self.B = False
        self.V = False
        self.N = False
        self.memory.initialize()

    def setRegister(self, register, value):
        """Sets a register/pointer.

        Increments cycles count by 1.

        Args:
            register (str): Register/pointer to set, see self.availableRegisters.
            value ([type]): Value to set.

        Raises:
            TypeError: Tried to access a register that is not in self.availableRegisters.
        """
        if register in self.availableRegisters:
            setattr(self, register, value)
            self.cycles_count += 1
        else:
            raise TypeError("{} register doesn't exist.".format(register))

    def getRegister(self, register):
        """Gets the value on a register/pointer.

        Increments cycles count by 1.

        Args:
            register (str): Register/pointer to get, see self.availableRegisters.

        Raises:
            TypeError: Tried to access a register that is not in self.availableRegisters.

        Returns:
            np.uint8 or np.uint16: Value on register/pointer.
        """
        if register in self.availableRegisters:
            self.cycles_count += 1
            return getattr(self, register)
        else:
            raise TypeError("{} register doesn't exist.".format(register))

    # Deals with Endianness
    def mergeWord(self, b0, b1):
        if self.LITTLE_ENDIAN:
            data = b0 | np.left_shift(b1, 8)
        else:
            data = b1 | np.left_shift(b0, 8)
        return data

    # Deals with Endianness
    def splitWord(self, word):
        if self.LITTLE_ENDIAN:
            b0 = word & 0xFF
            b1 = np.right_shift(word, 8)
        else:
            b1 = word & 0xFF
            b0 = np.right_shift(word, 8)
        return b0, b1

    def fetchByte(self):
        """Fetch next byte in memory.

        Decrements Program Counter by 1, increments cycles count by 1.

        Returns:
            np.uint8: Next byte on memory.
        """
        byte = self.readByte(self.PC)
        self.PC += 1
        return byte


    def fetchWord(self):
        """Fetch next word in memory.

        Decrements Program Counter by 2, increments cycles count by 2.

        Returns:
            np.uint16: Next word in memory.
        """
        # 6502 is little endian
        b0 = self.fetchByte()
        b1 = self.fetchByte()
        return self.mergeWord(b0, b1)

    def popByteFromStack(self):
        """Pops a byte from stack.

        Decrements Stack Pointer by 1, increments cycles count by 1.

        Returns:
            np.uint8: Byte on top of stack.
        """
        self.SP -= 1
        return self.readByte(self.SP)

    def popWordFromStack(self):
        """Pops a word from stack.

        Decrements Stack Pointer by 2, increments cycles count by 2.

        Returns:
            np.uint16: Word on top of stack.
        """
        b1 = self.popByteFromStack()
        b0 = self.popByteFromStack()
        return self.mergeWord(b0, b1)

    def writeByteToStack(self, byte):
        """Write a byte to the stack.

        Increments Stack Pointer by 1, increments cycles count by 1.

        Args:
            byte (np.uint8): Byte to write to stack.
        """
        self.writeByte(byte, self.SP)
        self.SP += 1

    def writeWordToStack(self, word):
        """Write a word to the stack.

        Increments Stack Pointer by 2, increments cycles count by 2.

        Args:
            byte (np.uint16): Word to write to stack.
        """
        b0, b1 = self.splitWord(word)
        self.writeByteToStack(b0)
        self.writeByteToStack(b1)

    def readByte(self, address):
        """Reads a byte from address in memory.

        Increments clycles count by 1.

        Args:
            address (np.uint16): Memory address to read.

        Returns:
            np.uint8: Byte on address.
        """
        self.cycles_count += 1
        return self.memory[address]

    def writeByte(self, byte, address):
        """Write a byte to address in memory.

        Args:
            byte (np.uint8): Byte to be written to memory.
            address (np.uint16): Address in memory to write byte.
        """
        self.memory[address] = byte
        self.cycles_count += 1

    def execute(self, cycles):
        start_cycle = self.cycles_count
        while self.cycles_count < (start_cycle + cycles):
            ins = self.fetchByte()
            try:
                CPU_OPT_CODES[ins](self)
            except KeyError:
                print("Instruction not handled {}".format(hex(ins)))