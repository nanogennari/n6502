# n6502

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-green.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html) [![Release](https://img.shields.io/badge/dynamic/json?color=blueviolet&label=Release&query=%24[0].name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2Fnanogennari%252Fn6502%2Frepository%2Ftags&style=flat-square)](https://gitlab.com/nanogennari/n6502/)

n6502 is a personal exercise on how processors work, is an 6502 emulator written in python.

n6502 is in it's early stages of development and is not functional yet.

## Installation

    git clone https://gitlab.com/nanogennari/n6502.git
    cd n6502
    python setup.py install